//@ts-check
'use strict';
const path = require('path');
const process = require('process');
const ZipPlugin = require('zip-webpack-plugin');
const rootDir = __dirname;
const workingDir = process.cwd();

module.exports = {
  target: 'node',
  entry: path.resolve(workingDir, 'src'),
  resolve: {
    extensions: ['.js', '.jsx', '.json', '.ts', '.tsx'],
    modules: [path.resolve(rootDir, 'node_modules'), path.resolve(workingDir, 'node_modules')],
    symlinks: false,
  },
  output: {
    libraryTarget: 'commonjs',
    path: path.resolve(workingDir, 'dist'),
    filename: 'index.js',
  },
  plugins: [
    new ZipPlugin({
      path: workingDir,
      filename: require(path.resolve(workingDir, 'package.json')).name,
    }),
  ],
  module: {
    rules: [
      {
        test: /\.(t|j)s(x?)$/,
        exclude: /node_modules/,
        use: {
          loader: 'ts-loader',
          options: {
            onlyCompileBundledFiles: true,
          },
        },
      },
    ],
  },
};
