# Readme
An example project how aws lambda structure can be made with yarn workspaces and lerna and jest testing.

## Build steps
The following build steps are configured. Note that in order to be runned for each package, the `package.json` must include the step being run from the root directory.

### Install
The *install* step installs the dependencies for all packages and stores it in the root directories `node_modules`.
If there are dependencies between packages, a symlink is created in `node_modules`.

```
yarn install
```

### Clean
The *clean* step removes the zipped package as well as the `dist` folder for each project.

```
yarn clean
```

### Build
The *build* step transpiles all of the typescript code and builds a single `index.js` file under each package's `dist` folder. Additionally it creates the zip deployment package.

```
yarn run build
```

### Test
The *test* step runs the tests for each package.

```
yarn test
```
