import {corsHeaders} from '@cygni/api-gw-utils';
import {APIGatewayEvent} from 'aws-lambda';
import axios from 'axios';

export const SHOWS_BASE_URL = 'http://api.tvmaze.com/shows';

export const handler = async (event: APIGatewayEvent) => {

  console.log(event.requestContext.requestId);

  let response;
  try {
    response = await axios.get(`${SHOWS_BASE_URL}/${event.pathParameters!.id}`)
  } catch (err) {
    console.log(err.response.data);
    return {
      statusCode: err.response.status,
      body: JSON.stringify({message: err.response.data.message}),
      headers: corsHeaders,
    }
  }

  return {
    statusCode: 200,
    headers: corsHeaders,
    body: JSON.stringify(response.data),
  };
};

