import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import {SHOWS_BASE_URL, handler} from '../';


// This sets the mock adapter on the default instance
const mock = new MockAdapter(axios);

it('should get a show object', async () => {

  const event : any =  {

    pathParameters: {id: '1'},
    requestContext: { requestId: '1'},
  };



  // Mock any GET request to /users
  mock.onGet(`${SHOWS_BASE_URL}/${event.pathParameters.id}`)
      .replyOnce(200, {
        id: event.pathParameters.id
      });
  
  const response = await handler(event);
  expect(response).toHaveProperty('body');
  expect(response).toHaveProperty('headers');
  expect(response).toHaveProperty('statusCode', 200);
});
